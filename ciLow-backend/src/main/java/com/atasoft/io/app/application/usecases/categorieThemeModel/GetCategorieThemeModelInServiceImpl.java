package com.atasoft.io.app.application.usecases.categorieThemeModel;

import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.app.domain.ports.input.categorieThemeModel.GetCategorieThemeModelInService;
import com.atasoft.io.app.domain.ports.output.CategorieThemeRepositoryPort;
import com.atasoft.io.utils.anotation.AppService;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

@AppService
@Slf4j
public class GetCategorieThemeModelInServiceImpl implements GetCategorieThemeModelInService {
    private final CategorieThemeRepositoryPort categorieThemeRepositoryPort;

    public GetCategorieThemeModelInServiceImpl(CategorieThemeRepositoryPort categorieThemeRepositoryPort) {
        this.categorieThemeRepositoryPort = categorieThemeRepositoryPort;
    }

    @Override
    public List<CategorieThemeModel> getCategorieList() {
        return this.categorieThemeRepositoryPort.getCategorieList();
    }

    @Override
    public Optional<CategorieThemeModel> getCategorieThemeModelById(Long categorieThemeId) throws NotFoundException {
        log.info("[getCategorieThemeModelById] delete categories ");
        if (categorieThemeId==null){
            log.debug("[getCategorieThemeModelById] categories  {} not existe ",categorieThemeId);
            throw new  NotFoundException("Categories not found");
        }
        return this.categorieThemeRepositoryPort.getCategorieThemeModelById(categorieThemeId);
    }
}
