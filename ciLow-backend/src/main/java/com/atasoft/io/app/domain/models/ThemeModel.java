package com.atasoft.io.app.domain.models;

public record ThemeModel(
        Long id,
        String libelle,
        String code,
        CategorieThemeModel categorieThemeModel
) {
}
