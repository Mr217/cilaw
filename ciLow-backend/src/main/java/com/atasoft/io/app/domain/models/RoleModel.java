package com.atasoft.io.app.domain.models;

public record RoleModel(
        Long id,
        String name,
        String code,
        String description
) {
}
