package com.atasoft.io.app.application.usecases.role;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.app.domain.ports.input.role.CreatedRoleInService;
import com.atasoft.io.app.domain.ports.output.RoleRepositoryPort;
import com.atasoft.io.utils.anotation.AppService;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@AppService
public class CreateRoleServiceImpl implements CreatedRoleInService {

    private final RoleRepositoryPort roleRepositoryPort;

    public CreateRoleServiceImpl(RoleRepositoryPort roleRepositoryPort) {
        this.roleRepositoryPort = roleRepositoryPort;
    }

    @Override
    public RoleModel save(RoleModel role) throws NotFoundException {
       log.info("[CreateRoleServiceImpl]");
        return roleRepositoryPort.save(role);
    }
}
