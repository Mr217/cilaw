package com.atasoft.io.app.application.usecases.categorieThemeModel;

import com.atasoft.io.app.domain.ports.input.categorieThemeModel.DeleteCategorieThemeModelInService;
import com.atasoft.io.app.domain.ports.output.CategorieThemeRepositoryPort;
import com.atasoft.io.utils.anotation.AppService;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;

@AppService
@Slf4j
public class DeleteCategorieThemeModelInServiceImpl implements DeleteCategorieThemeModelInService {
    private final CategorieThemeRepositoryPort categorieThemeRepositoryPort;

    public DeleteCategorieThemeModelInServiceImpl(CategorieThemeRepositoryPort categorieThemeRepositoryPort) {
        this.categorieThemeRepositoryPort = categorieThemeRepositoryPort;
    }

    @Override
    public void delete(Long categorieThemeModelId) throws NotFoundException {
       log.info("[delete] delete categories ");
        if (categorieThemeModelId==null){
            log.debug("[delete] categories  {} not existe ",categorieThemeModelId);
            throw new  NotFoundException("Categories not found");
        }
        this.categorieThemeRepositoryPort.delete(categorieThemeModelId);
    }
}
