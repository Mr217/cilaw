package com.atasoft.io.app.domain.ports.input.categorieThemeModel;

import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.utils.anotation.DomaineService;
import com.atasoft.io.utils.exceptions.NotFoundException;

@DomaineService
public interface UpdateCategorieThemeModelInService {
    CategorieThemeModel update(CategorieThemeModel categorieThemeModel) throws NotFoundException;
}
