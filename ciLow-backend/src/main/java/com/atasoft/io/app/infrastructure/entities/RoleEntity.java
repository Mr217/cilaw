package com.atasoft.io.app.infrastructure.entities;

import com.atasoft.io.app.domain.models.RoleModel;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ROLES")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(unique = true)
    private String code;
    private String description;

    public static RoleEntity fromDomainToModel(RoleModel roleModel){
        return new RoleEntity(
                roleModel.id(),
                roleModel.name(),
                roleModel.code(),
                roleModel.description()
        );
    }

    public RoleModel toDomainModel(){
        return new RoleModel(
                id,
                name,
                code,
                description
        );
    }
}
