package com.atasoft.io.app.application.usecases.role;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.app.domain.ports.input.role.GetRoleInService;
import com.atasoft.io.app.domain.ports.output.RoleRepositoryPort;
import com.atasoft.io.utils.anotation.AppService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;
@Slf4j
@AppService
public class GetRoleServiceImpl implements GetRoleInService {
    private final RoleRepositoryPort roleRepositoryPort;

    public GetRoleServiceImpl(RoleRepositoryPort roleRepositoryPort) {
        this.roleRepositoryPort = roleRepositoryPort;
    }

    @Override
    public List<RoleModel> getAllRoles() {
         log.info("[GetRoleServiceImpl] get role list");
        return this.roleRepositoryPort.getAllRoles();
    }

    @Override
    public Optional<RoleModel> getRoleById(Long id) {
        return this.roleRepositoryPort.getRoleById(id);
    }
}
