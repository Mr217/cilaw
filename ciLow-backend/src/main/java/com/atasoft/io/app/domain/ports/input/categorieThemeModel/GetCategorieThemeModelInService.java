package com.atasoft.io.app.domain.ports.input.categorieThemeModel;



import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.utils.exceptions.NotFoundException;

import java.util.List;
import java.util.Optional;

public interface GetCategorieThemeModelInService {
    List<CategorieThemeModel> getCategorieList();
    Optional<CategorieThemeModel> getCategorieThemeModelById(Long categorieThemeId) throws NotFoundException;
}
