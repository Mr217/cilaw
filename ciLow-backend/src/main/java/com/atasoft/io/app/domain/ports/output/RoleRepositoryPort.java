package com.atasoft.io.app.domain.ports.output;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.utils.anotation.DomaineService;
import com.atasoft.io.utils.exceptions.NotFoundException;

import java.util.List;
import java.util.Optional;

@DomaineService
public interface RoleRepositoryPort {
    RoleModel save(RoleModel role) throws NotFoundException;
    boolean deleteById(Long id);
    List<RoleModel> getAllRoles();
    Optional<RoleModel> getRoleById(Long id);
    RoleModel update(RoleModel role);
}
