package com.atasoft.io.app.application.services;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.app.domain.ports.input.role.CreatedRoleInService;
import com.atasoft.io.app.domain.ports.input.role.DeleteRoleInService;
import com.atasoft.io.app.domain.ports.input.role.GetRoleInService;
import com.atasoft.io.app.domain.ports.input.role.UpdateRoleInService;
import com.atasoft.io.app.domain.ports.output.RoleRepositoryPort;
import com.atasoft.io.app.infrastructure.repositories.JpaRoleEntityRepository;
import com.atasoft.io.utils.anotation.AppService;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;
@Slf4j
@AppService
public class RoleService  {


    private final CreatedRoleInService createdRoleInService;
    private final DeleteRoleInService deleteRoleInService;
    private final GetRoleInService getRoleInService;
    private final UpdateRoleInService updateRoleInService;

    public RoleService(CreatedRoleInService createdRoleInService, DeleteRoleInService deleteRoleInService, GetRoleInService getRoleInService, UpdateRoleInService updateRoleInService) {
        this.createdRoleInService = createdRoleInService;
        this.deleteRoleInService = deleteRoleInService;
        this.getRoleInService = getRoleInService;
        this.updateRoleInService = updateRoleInService;
    }


    public RoleModel save(RoleModel role) throws NotFoundException {
        if(role==null){
            throw new NotFoundException("This role not found");
        }
        return createdRoleInService.save(role);
    }


    public boolean deleteById(Long id) {
        this.deleteRoleInService.deleteById(id);
        return false;
    }


    public List<RoleModel> getAllRoles() {

        return getRoleInService.getAllRoles();
    }


    public Optional<RoleModel> getRoleById(Long id) {

        return getRoleInService.getRoleById(id);
    }


    public RoleModel update(RoleModel role) {

        try {
            return  this.updateRoleInService.update(role);
        } catch (NotFoundException e) {
            throw new RuntimeException(e);
        }
    }


}
