package com.atasoft.io.app.infrastructure.adapters;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.app.domain.ports.output.RoleRepositoryPort;
import com.atasoft.io.app.infrastructure.entities.RoleEntity;
import com.atasoft.io.app.infrastructure.repositories.JpaRoleEntityRepository;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class JpaRoleRepositoryAdapters implements RoleRepositoryPort {
    private final JpaRoleEntityRepository jpaRoleEntityRepository;

    @Override
    public RoleModel save(RoleModel role) throws NotFoundException {
        RoleEntity roleEntity=this.jpaRoleEntityRepository.save(RoleEntity.fromDomainToModel(role));
        return roleEntity.toDomainModel();
    }

    @Override
    public boolean deleteById(Long id) {
        this.jpaRoleEntityRepository.deleteById(id);
        return false;
    }

    @Override
    public List<RoleModel> getAllRoles() {
        List<RoleEntity> roleEntityList=this.jpaRoleEntityRepository.findAll();
        return roleEntityList.stream().map(RoleEntity::toDomainModel).collect(Collectors.toList());
    }

    @Override
    public Optional<RoleModel> getRoleById(Long id) {

        return this.jpaRoleEntityRepository.findById(id).map(RoleEntity::toDomainModel);
    }

    @Override
    public RoleModel update(RoleModel role) {
        RoleEntity roleEntity=this.jpaRoleEntityRepository.save(RoleEntity.fromDomainToModel(role));
        return roleEntity.toDomainModel();
    }
}
