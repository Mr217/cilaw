package com.atasoft.io.app.application.services;

import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.app.domain.ports.input.categorieThemeModel.CreatedCategorieThemeModelInService;
import com.atasoft.io.app.domain.ports.input.categorieThemeModel.DeleteCategorieThemeModelInService;
import com.atasoft.io.app.domain.ports.input.categorieThemeModel.GetCategorieThemeModelInService;
import com.atasoft.io.app.domain.ports.input.categorieThemeModel.UpdateCategorieThemeModelInService;
import com.atasoft.io.utils.anotation.AppService;
import com.atasoft.io.utils.exceptions.NotFoundException;

import java.util.List;

@AppService
public class CategoriesThemeService {
    private final CreatedCategorieThemeModelInService createdCategorieThemeModelInService;
    private final DeleteCategorieThemeModelInService deleteCategorieThemeModelInService;
    private final GetCategorieThemeModelInService getCategorieThemeModelInService;
    private final UpdateCategorieThemeModelInService updateCategorieThemeModelInService;

    public CategoriesThemeService(CreatedCategorieThemeModelInService createdCategorieThemeModelInService,
                                  DeleteCategorieThemeModelInService deleteCategorieThemeModelInService,
                                  GetCategorieThemeModelInService getCategorieThemeModelInService,
                                  UpdateCategorieThemeModelInService updateCategorieThemeModelInService) {
        this.createdCategorieThemeModelInService = createdCategorieThemeModelInService;
        this.deleteCategorieThemeModelInService = deleteCategorieThemeModelInService;
        this.getCategorieThemeModelInService = getCategorieThemeModelInService;
        this.updateCategorieThemeModelInService = updateCategorieThemeModelInService;
    }

    public List<CategorieThemeModel> getCategoriesThemesList(){
        return this.getCategorieThemeModelInService.getCategorieList();
    }

    public CategorieThemeModel saveCategoriesTheme(CategorieThemeModel categorieThemeModel) throws NotFoundException {
        return this.createdCategorieThemeModelInService.save(categorieThemeModel);
    }

    public CategorieThemeModel getCategorieThemeById(Long categoriesId) throws NotFoundException {
        return this.getCategorieThemeModelInService.getCategorieThemeModelById(categoriesId).get();
    }

    public CategorieThemeModel updateCategories(CategorieThemeModel categorieThemeModel) throws NotFoundException {
        return this.updateCategorieThemeModelInService.update(categorieThemeModel);
    }

    public void deleteCategorieThemeById(Long categoriesId) throws NotFoundException {
        this.deleteCategorieThemeModelInService.delete(categoriesId);
    }
}
