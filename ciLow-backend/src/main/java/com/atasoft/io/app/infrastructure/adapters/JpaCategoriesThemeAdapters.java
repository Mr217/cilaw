package com.atasoft.io.app.infrastructure.adapters;

import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.app.domain.ports.output.CategorieThemeRepositoryPort;
import com.atasoft.io.app.infrastructure.entities.CategoriesThemeEntity;
import com.atasoft.io.app.infrastructure.repositories.JpaCategoriesThemeEntityRepository;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Slf4j
@RequiredArgsConstructor
public class JpaCategoriesThemeAdapters implements CategorieThemeRepositoryPort {
   private final JpaCategoriesThemeEntityRepository jpaCategoriesThemeEntityRepository;



    @Override
    public CategorieThemeModel save(CategorieThemeModel categorieThemeModel) throws NotFoundException {
        log.info("[save] save categories");
        CategoriesThemeEntity categoriesThemeEntity=this.jpaCategoriesThemeEntityRepository.save(CategoriesThemeEntity.fromDomainToModel(categorieThemeModel));
        return categoriesThemeEntity.toDomainModel();
    }

    @Override
    public void delete(Long categorieThemeModelId) throws NotFoundException {
        log.info("[delete] delete categories id: {}", categorieThemeModelId);
            this.jpaCategoriesThemeEntityRepository.deleteById(categorieThemeModelId);
    }

    @Override
    public List<CategorieThemeModel> getCategorieList() {
        log.info("[getCategorieList] get list  categories");
        return this.jpaCategoriesThemeEntityRepository
                .findAll()
                .stream()
                .map(CategoriesThemeEntity::toDomainModel)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<CategorieThemeModel> getCategorieThemeModelById(Long categorieThemeId) throws NotFoundException {
        log.info("[getCategorieThemeModelById] get categorie id: {}", categorieThemeId);
        return this.jpaCategoriesThemeEntityRepository.findById(categorieThemeId).map(CategoriesThemeEntity::toDomainModel);
    }

    @Override
    public CategorieThemeModel update(CategorieThemeModel categorieThemeModel) throws NotFoundException {
        log.info("[update] update categories id: {}", categorieThemeModel.id());
        CategoriesThemeEntity categoriesThemeEntity=this.jpaCategoriesThemeEntityRepository.save(CategoriesThemeEntity.fromDomainToModel(categorieThemeModel));
        return categoriesThemeEntity.toDomainModel();
    }
}
