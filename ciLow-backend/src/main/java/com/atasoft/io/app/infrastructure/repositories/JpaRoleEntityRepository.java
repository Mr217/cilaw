package com.atasoft.io.app.infrastructure.repositories;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.app.infrastructure.entities.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface JpaRoleEntityRepository extends JpaRepository<RoleEntity,Long> {
}
