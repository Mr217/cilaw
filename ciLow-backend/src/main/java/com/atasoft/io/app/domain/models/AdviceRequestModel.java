package com.atasoft.io.app.domain.models;

import java.time.LocalDateTime;
import java.util.List;

public record AdviceRequestModel(
        Long id,
        String title,
        String description,
        UserModel userCustomer,
        List<UserModel> lawyer,
        UserModel principalLawyer,
        LocalDateTime createdDate
) {
}
