package com.atasoft.io.app.application.usecases.categorieThemeModel;

import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.app.domain.ports.input.categorieThemeModel.CreatedCategorieThemeModelInService;
import com.atasoft.io.app.domain.ports.output.CategorieThemeRepositoryPort;
import com.atasoft.io.utils.anotation.AppService;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;

@AppService
@Slf4j
public class CreatedCategorieThemeModelInServiceImpl implements CreatedCategorieThemeModelInService {
    private final CategorieThemeRepositoryPort categorieThemeRepositoryPort;

    public CreatedCategorieThemeModelInServiceImpl(CategorieThemeRepositoryPort categorieThemeRepositoryPort) {
        this.categorieThemeRepositoryPort = categorieThemeRepositoryPort;
    }

    @Override
    public CategorieThemeModel save(CategorieThemeModel categorieThemeModel) throws NotFoundException {

        log.info("[save] save categories ");
         if (categorieThemeModel==null){
             log.debug("[save] categories theme model not exist  ");
             throw new  NotFoundException("Categories not found");
         }
        return this.categorieThemeRepositoryPort.save(categorieThemeModel);
    }
}
