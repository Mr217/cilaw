package com.atasoft.io.app.application.usecases.categorieThemeModel;

import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.app.domain.ports.input.categorieThemeModel.UpdateCategorieThemeModelInService;
import com.atasoft.io.app.domain.ports.output.CategorieThemeRepositoryPort;
import com.atasoft.io.utils.anotation.AppService;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;

@AppService
@Slf4j
public class UpdateCategorieThemeModelInServiceImpl implements UpdateCategorieThemeModelInService {
    private final CategorieThemeRepositoryPort categorieThemeRepositoryPort;

    public UpdateCategorieThemeModelInServiceImpl(CategorieThemeRepositoryPort categorieThemeRepositoryPort) {
        this.categorieThemeRepositoryPort = categorieThemeRepositoryPort;
    }


    @Override
    public CategorieThemeModel update(CategorieThemeModel categorieThemeModel) throws NotFoundException {
        log.info("[update] update categories ");
        if (categorieThemeModel==null){
            log.debug("[update] categories theme model not exist  ");
            throw new  NotFoundException("Categories not found");
        }
        return this.categorieThemeRepositoryPort.update(categorieThemeModel);
    }
}
