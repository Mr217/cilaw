package com.atasoft.io.app.infrastructure.config;

import com.atasoft.io.utils.anotation.AppService;
import com.atasoft.io.utils.anotation.DomaineService;
import com.atasoft.io.utils.anotation.Stub;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import static org.springframework.context.annotation.FilterType.ANNOTATION;

@Configuration
@ComponentScan(
        basePackages = {"com.atasoft.io.app.application.*","com.atasoft.io.app.domain.*"},
        includeFilters = {@ComponentScan.Filter(type = ANNOTATION, classes = {AppService.class, DomaineService.class, Stub.class})}
)
public class DomainConfiguration {
}
