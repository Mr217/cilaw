package com.atasoft.io.app.domain.ports.input.role;



public interface DeleteRoleInService {
    boolean deleteById(Long id);
}
