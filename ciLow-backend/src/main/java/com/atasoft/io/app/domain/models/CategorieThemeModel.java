package com.atasoft.io.app.domain.models;

public record CategorieThemeModel(
        Long id,
        String libelle,
        String code
) {
}
