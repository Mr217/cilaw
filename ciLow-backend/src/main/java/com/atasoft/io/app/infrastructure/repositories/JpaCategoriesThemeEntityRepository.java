package com.atasoft.io.app.infrastructure.repositories;

import com.atasoft.io.app.infrastructure.entities.CategoriesThemeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaCategoriesThemeEntityRepository extends JpaRepository<CategoriesThemeEntity,Long> {
}
