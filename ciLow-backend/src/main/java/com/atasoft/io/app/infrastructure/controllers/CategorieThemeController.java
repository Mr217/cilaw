package com.atasoft.io.app.infrastructure.controllers;

import com.atasoft.io.app.application.services.CategoriesThemeService;
import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/categories-theme")
public class CategorieThemeController {
    private final CategoriesThemeService categoriesThemeService;

    @GetMapping("/list")
    public ResponseEntity<List<CategorieThemeModel>> getList(){
        log.info("[getList] get categories theme list");
        List<CategorieThemeModel> categorieThemeModels =this.categoriesThemeService.getCategoriesThemesList();
        return ResponseEntity.ok(categorieThemeModels);
    }

    @GetMapping("/{categorieId}")
    public ResponseEntity<CategorieThemeModel> getCategoriesTheme(@PathVariable Long categorieId){
        try {
            return ResponseEntity.ok(this.categoriesThemeService.getCategorieThemeById(categorieId));
        } catch (NotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<CategorieThemeModel> save(@RequestBody CategorieThemeModel categorieThemeModel){
        try {
            CategorieThemeModel categorieThem=this.categoriesThemeService.saveCategoriesTheme(categorieThemeModel);
            return new ResponseEntity<>(categorieThem, HttpStatus.CREATED);
        } catch (NotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/update")
    public ResponseEntity<CategorieThemeModel> update(@RequestBody CategorieThemeModel categorieThemeModel){
        try {
            CategorieThemeModel categorieThem=this.categoriesThemeService.updateCategories(categorieThemeModel);
            return new ResponseEntity<>(categorieThem, HttpStatus.ACCEPTED);
        } catch (NotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @DeleteMapping("/{categorieId}")
    public void delete(@PathVariable Long categorieId){
        try {
            this.categoriesThemeService.deleteCategorieThemeById(categorieId);
        } catch (NotFoundException e) {
            throw new RuntimeException(e);
        }

    }

}
