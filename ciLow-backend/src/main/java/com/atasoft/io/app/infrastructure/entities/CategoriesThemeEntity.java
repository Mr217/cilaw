package com.atasoft.io.app.infrastructure.entities;

import com.atasoft.io.app.domain.models.CategorieThemeModel;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "CATEGORIES_THEME")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoriesThemeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String libelle;
    @Column(unique = true)
    private String code;

    public static CategoriesThemeEntity fromDomainToModel(CategorieThemeModel categories){
        return new CategoriesThemeEntity(
                categories.id(),
                categories.libelle(),
                categories.code()
        );
    }

    public CategorieThemeModel toDomainModel(){
        return new CategorieThemeModel(
                id,
                libelle,
                code
        );
    }
}
