package com.atasoft.io.app.domain.ports.input.role;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.utils.anotation.DomaineService;

import java.util.List;
import java.util.Optional;


public interface GetRoleInService {
    List<RoleModel> getAllRoles();
    Optional<RoleModel> getRoleById(Long id);
}
