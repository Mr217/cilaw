package com.atasoft.io.app.domain.ports.output;

import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.utils.anotation.DomaineService;
import com.atasoft.io.utils.exceptions.NotFoundException;

import java.util.List;
import java.util.Optional;

@DomaineService
public interface CategorieThemeRepositoryPort {
    CategorieThemeModel save(CategorieThemeModel categorieThemeModel) throws NotFoundException;
    void delete(Long categorieThemeModelId) throws NotFoundException;

    List<CategorieThemeModel> getCategorieList();
    Optional<CategorieThemeModel> getCategorieThemeModelById(Long categorieThemeId) throws NotFoundException;
    CategorieThemeModel update(CategorieThemeModel categorieThemeModel) throws NotFoundException;
}
