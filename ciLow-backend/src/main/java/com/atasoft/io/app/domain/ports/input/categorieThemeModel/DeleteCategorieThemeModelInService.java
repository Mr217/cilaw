package com.atasoft.io.app.domain.ports.input.categorieThemeModel;

import com.atasoft.io.utils.exceptions.NotFoundException;


public interface DeleteCategorieThemeModelInService {
    void delete(Long categorieThemeModelId) throws NotFoundException;
}
