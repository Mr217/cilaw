package com.atasoft.io.app.domain.models;

public record UserModel(
        Long id,
        String firstName,
        String lastName,
        String username,
        String tel,
        String email,
        RoleModel role
) {
}
