package com.atasoft.io.app.application.usecases.role;

import com.atasoft.io.app.domain.ports.input.role.DeleteRoleInService;
import com.atasoft.io.app.domain.ports.output.RoleRepositoryPort;
import com.atasoft.io.utils.anotation.AppService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AppService
public class DeleteRoleServiceImpl implements DeleteRoleInService {
    private final RoleRepositoryPort roleRepositoryPort;

    public DeleteRoleServiceImpl(RoleRepositoryPort roleRepositoryPort) {
        this.roleRepositoryPort = roleRepositoryPort;
    }

    @Override
    public boolean deleteById(Long id) {
        return roleRepositoryPort.deleteById(id);
    }
}
