package com.atasoft.io.app.domain.ports.input.role;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.utils.anotation.DomaineService;
import com.atasoft.io.utils.exceptions.NotFoundException;


public interface UpdateRoleInService {
    RoleModel update(RoleModel role) throws NotFoundException;
}
