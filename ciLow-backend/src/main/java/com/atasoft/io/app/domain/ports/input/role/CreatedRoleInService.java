package com.atasoft.io.app.domain.ports.input.role;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.utils.exceptions.NotFoundException;


public interface CreatedRoleInService {
    RoleModel save(RoleModel role) throws NotFoundException;
}
