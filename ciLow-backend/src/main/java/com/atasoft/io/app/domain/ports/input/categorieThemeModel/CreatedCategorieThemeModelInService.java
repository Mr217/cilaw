package com.atasoft.io.app.domain.ports.input.categorieThemeModel;

import com.atasoft.io.app.domain.models.CategorieThemeModel;
import com.atasoft.io.utils.exceptions.NotFoundException;


public interface CreatedCategorieThemeModelInService {
    CategorieThemeModel save(CategorieThemeModel categorieThemeModel) throws NotFoundException;
}
