package com.atasoft.io.app.infrastructure.controllers;

import com.atasoft.io.app.application.services.RoleService;
import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.utils.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/role")
@RequiredArgsConstructor
public class RoleController {
    private final RoleService roleService;



    @GetMapping("/list")
    public ResponseEntity<List<RoleModel>> listRole(){
        return ResponseEntity.ok(roleService.getAllRoles());
    }

    @PostMapping("/create")
    public ResponseEntity<RoleModel> save(@RequestBody RoleModel roleModel){
        try {
            RoleModel role=roleService.save(roleModel);
            return new ResponseEntity<>(role, HttpStatus.CREATED);
        }catch (NotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/{roleId}")
    public ResponseEntity<RoleModel> getRoleByID(@PathVariable Long roleId){
        RoleModel roleModel=roleService.getRoleById(roleId).get();
        return ResponseEntity.ok(roleModel);
    }

    @DeleteMapping("/{roleId}")
    public void deleteRole(@PathVariable Long roleId){
        roleService.deleteById(roleId);
    }

    @PutMapping("/update")
    public ResponseEntity<RoleModel> update(@RequestBody RoleModel roleModel){
        RoleModel role=roleService.update(roleModel);
        return new ResponseEntity<>(role, HttpStatus.ACCEPTED);
    }

}
