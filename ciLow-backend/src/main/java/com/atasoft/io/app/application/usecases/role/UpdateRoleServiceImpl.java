package com.atasoft.io.app.application.usecases.role;

import com.atasoft.io.app.domain.models.RoleModel;
import com.atasoft.io.app.domain.ports.input.role.UpdateRoleInService;
import com.atasoft.io.app.domain.ports.output.RoleRepositoryPort;
import com.atasoft.io.utils.anotation.AppService;

@AppService
public class UpdateRoleServiceImpl implements UpdateRoleInService {
    private final RoleRepositoryPort roleRepositoryPort;

    public UpdateRoleServiceImpl(RoleRepositoryPort roleRepositoryPort) {
        this.roleRepositoryPort = roleRepositoryPort;
    }

    @Override
    public RoleModel update(RoleModel role) {
        return this.roleRepositoryPort.update(role);
    }
}
