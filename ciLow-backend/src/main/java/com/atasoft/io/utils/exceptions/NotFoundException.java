package com.atasoft.io.utils.exceptions;

public class NotFoundException extends Exception{
    public NotFoundException(String message) {
        super(message);
    }
}
