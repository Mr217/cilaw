package com.atasoft.io.utils;

import org.modelmapper.ModelMapper;

public class MapperUtil<S, D> {
    private static final ModelMapper modelMapper = new ModelMapper();
    private final Class<S> sourceType;
    private final Class<D> destinationType;

    public MapperUtil(Class<S> sourceType, Class<D> destinationType) {
        this.sourceType = sourceType;
        this.destinationType = destinationType;
    }

    public static <S, D> D convert(S source, Class<S> sourceType, Class<D> destinationType) {
        return modelMapper.map(source, destinationType);
    }
}
